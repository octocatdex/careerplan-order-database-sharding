集成的是ionicons字体图标。
可用图标可在：
https://ionicframework.com/docs/v3/ionicons/
查看。
使用时需要加上'ion-'作为前缀，例如在上面网站找到个图标用法写的是md-umbrella，那么在按钮中应该写成：
<q-btn  icon="ion-md-umbrella"/>
不然无法生效，可用图标展示网站的写法是因为他自己封装了个<ion-icon>标签，但实际上在这个字体的css里它的每一个图标的类名前面都有'icon-'，所以我们在用的时候必须加'ion-'前缀！