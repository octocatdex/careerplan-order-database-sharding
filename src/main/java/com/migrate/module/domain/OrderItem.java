package com.migrate.module.domain;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 订单详情
 *
 * @author zhonghuashishan
 */
@Data
public class OrderItem {

    private Long id;
    /**
     * 订单号
     */
    private String orderNo;
    /**
     * 商品ID
     */
    private Long productId;
    /**
     *  商品分类ID
     */
    private Long categoryId;
    /**
     *  商品购买数量
     */
    private Integer goodsNum;
    /**
     * 商品单价
     */
    private BigDecimal goodsPrice;
    /**
     * 商品总价
     */
    private BigDecimal goodsAmount;
    /**
     *  商品优惠金额
     */
    private BigDecimal discountAmount;
    /**
     *  活动ID
     */
    private Long discountId;
    /**
     *  商品名称
     */
    private String productName;
    /**
     *  商品图片
     */
    private String productPictureUrl;

    private Long createUser;

    private Long updateUser;

    private Date createTime;

    private Date updateTime;

    private Integer deleteFlag;

}
