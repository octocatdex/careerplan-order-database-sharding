package com.migrate.module.mapper.db1;

import com.migrate.module.domain.EtlProgress;
import com.migrate.module.domain.RangeScroll;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * 订单详情表数据库操作
 *
 * @author zhonghuashishan
 */
public interface OrderItemDetail01Mapper {

    /**
     * 获取当前的订单表的总数据量
     * @param etlProgress 查询条件
     * @return  总数据量
     */
    Integer countOrderInfo(EtlProgress etlProgress);

    /**
     * 按指定条件滚动拉取数据
     * @param rangeScroll  查询条件
     * @return 需要迁移的数据集合
     */
    List<Map<String, Object>> queryInfoList(RangeScroll rangeScroll);

    /**
     * 取该表的最小更新时间
     * @return 时间
     */
    Date queryMinDate();

    /**
     * 查询最小的订单号
     * @param rangeScroll 查询参数
     * @return 最小的订单号
     */
    String queryMinOrderNo(RangeScroll rangeScroll);
}
